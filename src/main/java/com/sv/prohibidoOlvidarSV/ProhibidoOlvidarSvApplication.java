package com.sv.prohibidoOlvidarSV;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/* Metodo main para hacer correr la aplicacion  
Anotaciones de configuracion y de nuestro Group Id que 
es donde se ubican nuestros modulos	*/

@Configuration
@ComponentScan("com.sv")
@SpringBootApplication
public class ProhibidoOlvidarSvApplication extends SpringBootServletInitializer{
	
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ProhibidoOlvidarSvApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ProhibidoOlvidarSvApplication.class, args);
	}

}
