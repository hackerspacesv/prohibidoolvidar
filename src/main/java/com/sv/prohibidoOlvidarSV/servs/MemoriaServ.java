package com.sv.prohibidoOlvidarSV.servs;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sv.prohibidoOlvidarSV.utils.AbsMemoria;
import com.sv.prohibidoOlvidarSV.utils.MemoriaRepository;
import com.sv.prohibidoOlvidarSV.beans.Memoria;

/** Mi clase abstracta que al tener la anotacion de servicio lo convierte en un 
servicio */


@Service
public class MemoriaServ implements AbsMemoria<Memoria>{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	MemoriaRepository memo;

	@Override
	public List<Memoria> gets() {
		
		return memo.findAll();
	}

	@Override
	public Memoria getById(Long id) {
		
		return memo.getOne(id);
	}
	
	@Override
	public List<Memoria> getByNext() {

	     return  entityManager.createQuery("SELECT  M FROM Memoria M WHERE (M.yes_vote + M.no_vote) < 1",
	             Memoria.class).setMaxResults(1).getResultList();
	}
	
	@Override
	public List<Memoria> getByTwenty() {
		
		return entityManager.createQuery("SELECT  M FROM Memoria M",
	             Memoria.class).setMaxResults(20).getResultList();
	}
	

	@Override
	@Transactional
	public Memoria registry(Memoria r) {

		return memo.save(r);
	}

	@Override
	public void clean(Memoria e) {
		
		memo.delete(e);
	}

}
